import React, { Component } from 'react';
import { Pagetitle } from 'Lib/Subheader'
import Header from 'Lib/Header'
import Sticky from 'Lib/Sticky'

class App extends Component {
  render() {
    return (
      <div>
        <Sticky>
          <Header menu={true} />
          <Pagetitle title={'UserAccount'} backButton={false} />
        </Sticky>
      </div>
    );
  }
}

export default App;
